(ns site.system.views
  (:require [re-frame.core :as re-frame]
    [reagent.core :as r]
    [site.app.views.main :as main-view]))

                                        ; Example with various components
(defn get-panel []
  [main-view/panel])
