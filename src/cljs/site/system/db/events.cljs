(ns site.system.db.events
    (:require [re-frame.core :as re-frame]
              [site.app.db :as db]))

(re-frame/reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))
