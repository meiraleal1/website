(ns site.system.theme
  (:require [cljs-react-material-ui.core :as mui]))

(def typography (aget js/MaterialUIStyles "typography"))
(def spacing (aget js/MaterialUIStyles "spacing"))
(def z-index (aget js/MaterialUIStyles "zIndex"))

(defn flex []
  {:display "flex"
    :flexWrap "wrap"})

(defn flex-align [direction]
  {:alignItems direction
    :justifyContent direction})

(defn align [direction]
  {:textAlign direction})

(defn box
  ([]
    (box nil))
  ([width]
    {:flex (str "1 " width)
      :marginLeft "auto"
      :marginRight "auto"
      :minWidth 0}))

(defn visibility [display]
  (let [display (if (= true display) "block" "none")]
    {:flex "1"
      :minWidth 0
      :display display}))

(defn margin
  ([value]
    {:margin value})
  ([horizontal vertical]
    {:marginRight horizontal
      :marginLeft horizontal
      :marginTop vertical
      :marginBottom vertical}))

(defn horizontal-margin [value]
  {:marginLeft value
    :marginRight value})

(defn vertical-margin [value]
  {:marginTop value
    :marginBottom value})

(defn padding
  ([value]
    {:padding value})
  ([horizontal vertical]
    {:paddingRight horizontal
      :paddingLeft horizontal
      :paddingTop vertical
      :paddingBottom vertical}))

(defn horizontal-padding [value]
  {:paddingLeft value
    :paddingRight value})

(defn vertical-padding [value]
  {:paddingTop value
    :paddingBottom value})

(defn header [color]
  {:cursor "pointer"
    :color (aget typography "textFullWhite")
    :fontSize 22
    :lineHeight (str (aget spacing "desktopKeylineIncrement") "px")
    :fontWeight (aget typography "fontWeightLight")
    :textAlign "center"
    :height 56
    :backgroundColor (mui/color color)})

(defn text-color [color]
  {:color (mui/color color)})

(defn bold [weight]
  {:fontWeight weight})
