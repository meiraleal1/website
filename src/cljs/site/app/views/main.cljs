(ns site.app.views.main
  (:require
    [reagent.core :as r]
    [re-frame.core :as rf]
    [cljs-react-material-ui.core :refer [get-mui-theme color]]
    [cljs-react-material-ui.reagent :as ui]
    [cljs-react-material-ui.icons :as ic]
    [site.app.style :as style]
    [site.components.menu.ui :as menu]))

(defn app-bar-menu []
  [ui/mui-theme-provider {:mui-theme (get-mui-theme style/default)}
    [:div
      [ui/icon-button
        [ic/action-search]]]])

(defn panel []
  [ui/mui-theme-provider {:mui-theme (get-mui-theme style/default)}
    [:div.body {:style {:padding-left "230"}}
      [ui/app-bar {:show-menu-icon-button false
                    :icon-style-right {:color "#ffffff"}
                    :icon-element-right (r/as-element [app-bar-menu])}]
      [menu/main]

      ]])
