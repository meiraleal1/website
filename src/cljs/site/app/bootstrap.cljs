(ns site.app.bootstrap
  (:require
    [cljsjs.material-ui]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [site.system.db.events]
    [site.system.db.subs]
    [site.system.views :as views]
    [site.app.config :as config]))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn system []
    [views/get-panel])

(defn mount-root []
  (reagent/render [system]
    (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
