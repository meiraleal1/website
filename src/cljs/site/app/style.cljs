(ns site.app.style
  (:require [cljs-react-material-ui.core :as mui]))

(def default {:pallete {}
               :appBar {:height 57
                         :color  (mui/color "blue600")
                         :textColor "#FFFFFF"}
               :drawer {:width 230
                         :color  (mui/color "grey900")}
               :raisedButton {:primaryColor (mui/color "blue600")}})
