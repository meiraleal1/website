(ns site.components.menu.style)

(def menu {:backgroundColor "#333333"})

(def menu-item {:color "#ffffff"
                 :fontSize 14
                 :display "block"})

(def user-profile {:padding "15px 0 15px 10px"
                    :backgroundImage  "url(../images/material_bg.png)"
                    :height 45})

(def user-avatar {:float "left"
                   :display "block"
                   :marginRight 15
                   :boxShadow "0px 0px 0px 8px rgba(0,0,0,0.2)"})

(def user-name {:paddingTop 12
                 :display "block"
                 :color "white"
                 :fontWeight 300
                 :textShadow "1px 1px #444"})
