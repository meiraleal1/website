(ns site.components.menu.ui
  (:require
    [reagent.core :as r]
    [re-frame.core :as rf]
    [cljs-react-material-ui.reagent :as ui]
    [site.system.theme :as theme]
    [site.components.menu.style :as style]))

(defn main []
  (let [name "Alan Meira"
         email "alan@engarte.com"]
    [ui/drawer {:open true
                 :docked true
                 :swipe-area-width nil
                 :style style/menu}
      [:div
        [:div {:style (theme/header "blue600")}
          [:a {:href "#/index.html" :style (theme/text-color "grey50")} "alipio.digital"]]
        [:div {:style style/user-profile}
          [:span {:style style/user-name}
            name [:small (str " (" email ")")]]]
        ]]))
