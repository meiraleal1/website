(defproject site "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0-alpha14"]
                  [org.clojure/clojurescript "1.9.293"]
                  [reagent "0.6.0" :exclusions [org.clojure/tools.reader cljsjs/react]]
                  [re-frame "0.8.0"]
                  [cljs-react-material-ui "0.2.30"]]

  :plugins [[lein-cljsbuild "1.1.4"]]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"
                                     "test/js"]

  :figwheel {:css-dirs ["resources/public/css"]}

  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

  :profiles
  {:dev
    {:dependencies [[binaryage/devtools "0.8.2"]
                     [figwheel-sidecar "0.5.7"]
                     [com.cemerick/piggieback "0.2.1"]]

      :plugins      [[lein-figwheel "0.5.7"]
                      [lein-doo "0.1.7"]
                      [cider/cider-nrepl "0.13.0"]]
      }}

  :cljsbuild
  {:builds
    [{:id           "dev"
       :source-paths ["src/cljs"]
       :figwheel     {:on-jsload "site.app.bootstrap/mount-root"}
       :compiler     {:main                 site.app.bootstrap
                       :output-to            "resources/public/js/compiled/app.js"
                       :output-dir           "resources/public/js/compiled/out"
                       :asset-path           "js/compiled/out"
                       :source-map-timestamp true
                       :preloads             [devtools.preload]
                       :external-config      {:devtools/config {:features-to-install :all}}
                       }}

      {:id           "min"
        :source-paths ["src/cljs"]
        :compiler     {:main            site.app.bootstrap
                        :output-to       "resources/public/js/compiled/app.js"
                        :optimizations   :advanced
                        :closure-defines {goog.DEBUG false}
                        :pretty-print    false}}

      {:id           "test"
        :source-paths ["src/cljs" "test/cljs"]
        :compiler     {:main          site.runner
                        :output-to     "resources/public/js/compiled/test.js"
                        :output-dir    "resources/public/js/compiled/test/out"
                        :optimizations :none}}
      ]}

  )
